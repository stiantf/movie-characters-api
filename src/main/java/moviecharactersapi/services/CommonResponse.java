package moviecharactersapi.services;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class CommonResponse {
    public Object data;
    public String message;
    public String error;
    public HttpStatus status;
}
