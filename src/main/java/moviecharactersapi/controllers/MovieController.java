package moviecharactersapi.controllers;

import moviecharactersapi.Repository.movie.MovieRepository;
import moviecharactersapi.models.Movie;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
public class MovieController {
    //Configure endpoint to manage crud
    private final MovieRepository movieRepository;

    public MovieController(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    //Display info about movie page to user
    @RequestMapping(value="/api/movie",method=RequestMethod.GET)
    public String welcomeToMovies() { return movieRepository.welcomeToMovies(); }

    //Get all movies
    @RequestMapping(value="/api/movie/all", method = RequestMethod.GET)
    public ArrayList<Movie> getAllMovies() { return movieRepository.getAllMovies(); }

    //Get all movies in a franchise
    @RequestMapping(value="/api/movie/{franchiseId}", method = RequestMethod.GET)
    public ArrayList<Movie> getAllFranchiseMovies(@PathVariable int franchiseId) { return movieRepository.getAllFranchiseMovies(franchiseId); }

    //Delete movie from DB
    @RequestMapping(value="/api/movie/delete/{movieId}", method = RequestMethod.GET)
    public String deleteMovie(@PathVariable int movieId) { return movieRepository.deleteMovie(movieId); }

    //Update movie in DB
    @RequestMapping(value="/api/movie/update/{movieId}/{movieTitle}/{genre}/{releaseYear}/{director}/{picture}/{trailer}/{franchiseId}", method = RequestMethod.GET)
    public String updateMovie(@PathVariable int movieId,
                              @PathVariable String movieTitle,
                              @PathVariable String genre,
                              @PathVariable int releaseYear,
                              @PathVariable String director,
                              @PathVariable String picture,
                              @PathVariable String trailer,
                              @PathVariable int franchiseId) {
        return movieRepository.updateMovie(movieId, movieTitle, genre, releaseYear, director, picture, trailer, franchiseId);
    }

    //Create a new movie in the DB
    @RequestMapping(value="/api/movie/create/{movieTitle}/{genre}/{releaseYear}/{director}/{picture}/{trailer}/{franchiseId}", method = RequestMethod.GET)
    public String createMovie(@PathVariable String movieTitle,
                              @PathVariable String genre,
                              @PathVariable int releaseYear,
                              @PathVariable String director,
                              @PathVariable String picture,
                              @PathVariable String trailer,
                              @PathVariable int franchiseId) {
        return movieRepository.createMovie(movieTitle, genre, releaseYear, director, picture, trailer, franchiseId);
    }
}
