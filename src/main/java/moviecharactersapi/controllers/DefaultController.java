package moviecharactersapi.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {
    @RequestMapping(value="/", method = RequestMethod.GET)
    public String homeScreen() {
        String header = "----------- HOME PAGE -----------";
        String character = "</br>Add /api/characters to the URL for more character related commands";
        String movie = "</br>Add /api/movie to the URL for more movie related commands";
        String franchise = "</br>Add /api/franchise to the URL for more franchise related commands";
        return header + character + movie + franchise;
    }

    @RequestMapping(value="/eror", method = RequestMethod.GET)
    public String error404() {
        String header = "----------- ERROR 404 -----------";
        String link = "</br><img src='https://http.cat/404'>";
        String statement = "</br>Whoa oh, cheerio!";
        String errorMessage = "</br>Your url could not be reached! Go back, double check your data and try again";
        return header + link + statement + errorMessage;
    }

}
