package moviecharactersapi.controllers;

import moviecharactersapi.Repository.franchise.FranchiseRepository;
import moviecharactersapi.models.Franchise;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class FranchiseController {
    //Configure endpoint to manage crud
    private final FranchiseRepository franchiseRepository;

    public FranchiseController(FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }

    //Display info about the franchise page to user
    @RequestMapping(value="/api/franchise",method=RequestMethod.GET)
    public String welcomeToFranchise() { return franchiseRepository.welcomeToFranchise(); }

    //Get all franchises
    @RequestMapping(value="/api/franchise/all", method = RequestMethod.GET)
    public ArrayList<Franchise> getAllFranchiseCharacters() { return franchiseRepository.getAllFranchises(); }

    //Delete a franchise from the DB
    @RequestMapping(value="/api/franchise/delete/{franchiseId}", method=RequestMethod.GET)
    public String deleteFranchise(@PathVariable int franchiseId) { return franchiseRepository.deleteFranchise(franchiseId); }

    //Update a franchise in the DB
    @RequestMapping(value="api/franchise/update/{franchiseId}/{name}/{description}", method=RequestMethod.GET)
    public String updateFranchise(@PathVariable int franchiseId,
                                  @PathVariable String name,
                                  @PathVariable String description)
    { return franchiseRepository.updateFranchise(franchiseId, name, description); }

    //Create a franchise in the DB
    @RequestMapping(value="api/franchise/create/{name}/{description}", method=RequestMethod.GET)
    public String createFranchise(@PathVariable String name,
                                  @PathVariable String description)
    { return franchiseRepository.createFranchise(name, description); }
}
