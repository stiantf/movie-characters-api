package moviecharactersapi.controllers;

import com.fasterxml.jackson.annotation.JsonGetter;
import moviecharactersapi.Repository.character.CharacterRepository;
import moviecharactersapi.models.Character;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CharacterController {
    //Configure endpoint to manage crud
    private final CharacterRepository characterRepository;

    public CharacterController(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    //Display info about the character page to user
    @RequestMapping(value="/api/characters",method=RequestMethod.GET)
    public ResponseEntity<String> welcomeToCharacters() {
        String characters = characterRepository.welcomeToCharacters();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(characters, resp);
    }

    //Get all characters
    @JsonGetter
    @RequestMapping(value="/api/characters/all", method=RequestMethod.GET)
    public ResponseEntity<List<Character>> getAllCharacters() {
        List<Character> characters = characterRepository.getAllCharacters();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(characters, resp);
    }

    //Get all characters in a movie
    @RequestMapping(value="/api/characters/movie/{movieId}", method = RequestMethod.GET)
    public ArrayList<Character> getAllMovieCharacters(@PathVariable int movieId) { return characterRepository.getAllMovieCharacters(movieId); }

    //Get all characters in a franchise
    @RequestMapping(value="api/characters/franchise/{franchiseId}", method = RequestMethod.GET)
    public ArrayList<Character> getAllFranchiseCharacters(@PathVariable int franchiseId) { return characterRepository.getAllFranchiseCharacters(franchiseId); }

    //Delete character from DB
    @RequestMapping(value = "/api/characters/delete/{characterId}", method = RequestMethod.GET)
    public String deleteCharacter(@PathVariable int characterId) { return characterRepository.deleteCharacter(characterId); }

    //Update character in DB
    @RequestMapping(value = "/api/characters/update/{characterId}/{fullName}/{alias}/{gender}/{picture}/{movieId}", method = RequestMethod.GET)
    public String updateCharacter(@PathVariable int characterId,
                                @PathVariable String fullName,
                                @PathVariable String alias,
                                @PathVariable String gender,
                                @PathVariable String picture,
                                @PathVariable int movieId) {
        return characterRepository.updateCharacter(characterId, fullName, alias, gender, picture, movieId); }

    //Create new character in DB
    @RequestMapping(value="/api/characters/create/{fullName}/{alias}/{gender}/{picture}/{movieId}", method = RequestMethod.GET)
        public String createCharacter(@PathVariable String fullName,
                                    @PathVariable String alias,
                                    @PathVariable String gender,
                                    @PathVariable String picture,
                                    @PathVariable int movieId) {
        return characterRepository.createCharacter(fullName, alias, gender, picture, movieId); }
}
