package moviecharactersapi.Startup;
import moviecharactersapi.Repository.character.CharacterRepository;
import moviecharactersapi.Repository.franchise.FranchiseRepository;
import moviecharactersapi.Repository.movie.MovieRepository;
import moviecharactersapi.models.Character;
import moviecharactersapi.models.Franchise;
import moviecharactersapi.models.Movie;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AppStartRunner  implements ApplicationRunner {
    private final CharacterRepository characterRepository;
    private final MovieRepository movieRepository;
    private final FranchiseRepository franchiseRepository;

    // Dummy data
    //Characters
    Character stifler = new Character(1, "Seann William Scott", "Stifler", "Male", "https://en.wikipedia.org/wiki/File:Seann_William_Scott_2012.jpg", 1, 1);
    Character jim = new Character(2, "Jason Biggs", "Jim Levenstein", "Male", "https://en.wikipedia.org/wiki/File:Jason_Biggs_at_Paley_Fest_Orange_Is_The_New_Black.jpg", 1, 1);
    Character chris = new Character(3, "Frederick Christopher Klein", "Chris 'Oz'", "Male", "https://en.wikipedia.org/wiki/File:Chris_Klein_2012.jpg", 1, 1);
    Character michelle = new Character(4, "Alyson Lee Hannigan", "Michelle", "Female", "https://en.wikipedia.org/wiki/File:Alyson_Hannigan,_2013-07-20_(cropped).jpg", 1, 1);
    Character finch = new Character(5, "Eddie Kaye Thomas", "Finch", "Male", "https://en.wikipedia.org/wiki/File:Eddie_Kaye_Thomas_by_Gage_Skidmore.jpg", 1, 1);

    Character jonah = new Character(6, "Jonah Hill", "Morton Schmidt", "Male", "https://en.wikipedia.org/wiki/File:Jonah_Hill-4939_(cropped)_(cropped).jpg", 5, 2);
    Character channing = new Character(7, "Channing Tatum", "MY NAMES JEFF", "Male", "https://en.wikipedia.org/wiki/File:Channing_Tatum_(18842022673).jpg", 5, 2);


    //Movies
    Movie AmericanPie1 = new Movie(1, "American Pie", "Comedy", 1999, "Paul Weitz", "https://en.wikipedia.org/wiki/File:American_Pie1.jpg", "youtube", 1);
    Movie AmericanPie2 = new Movie(2, "American Pie 2", "Comedy", 2001, "James B. Rogers", "https://en.wikipedia.org/wiki/File:Americanpie2.jpg", "youtube", 1);
    Movie AmericanPie3 = new Movie(3, "American Wedding", "Comedy", 2003, "Jesse Dylan", "https://en.wikipedia.org/wiki/File:American_Wedding_movie.jpg", "youtube", 1);
    Movie AmericanPie4 = new Movie(4, "American Reunion", "Comedy", 2012, "Jon Hurwitz", "https://en.wikipedia.org/wiki/File:American_Reunion_Film_Poster.jpg", "youtube", 1);

    Movie JumpStreet21 = new Movie(5, "21 Jump Street", "Comedy", 2012, "Phil Lord", "https://en.wikipedia.org/wiki/File:21JumpStreetfilm.jpg", "youtube", 2);
    Movie JumpStreet22 = new Movie(6, "22 Jump Street", "Comedy", 2014, "Phil Lord", "https://en.wikipedia.org/wiki/File:22_Jump_Street_Poster.png", "youtube", 2);

    //Franchises
    Franchise AmericanPie = new Franchise(1, "American Pie", "American Pie is a film series consisting of four sex comedy films.");
    Franchise JumpStreet = new Franchise(2, "Jump Street", "Jump Street is an American multimedia franchise that commenced in 1987.");

    public AppStartRunner(CharacterRepository characterRepository, MovieRepository movieRepository, FranchiseRepository franchiseRepository) {
        this.characterRepository = characterRepository;
        this.movieRepository = movieRepository;
        this.franchiseRepository = franchiseRepository;
    }

    public void run(ApplicationArguments args){
        //Adding franchises
        franchiseRepository.saveFranchise(AmericanPie);
        franchiseRepository.saveFranchise(JumpStreet);

        //Adding movies
        movieRepository.saveMovie(AmericanPie1);
        movieRepository.saveMovie(AmericanPie2);
        movieRepository.saveMovie(AmericanPie3);
        movieRepository.saveMovie(AmericanPie4);
        movieRepository.saveMovie(JumpStreet21);
        movieRepository.saveMovie(JumpStreet22);




        //Adding characters
        characterRepository.saveCharacter(stifler);
        characterRepository.saveCharacter(jim);
        characterRepository.saveCharacter(chris);
        characterRepository.saveCharacter(michelle);
        characterRepository.saveCharacter(finch);
        characterRepository.saveCharacter(jonah);
        characterRepository.saveCharacter(channing);
    }
}
