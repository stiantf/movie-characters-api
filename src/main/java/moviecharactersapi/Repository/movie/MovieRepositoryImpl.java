package moviecharactersapi.Repository.movie;

import moviecharactersapi.models.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;

@Repository
public class MovieRepositoryImpl implements MovieRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    //Introduction to movies page
    public String welcomeToMovies() {
        String header = "----------- MOVIE PAGE -----------";
        String intro = "</br>Add the following to your URL to access commands!";
        String getMovies = "</br>/all - See all the movies in the DB";
        String getFranchiseMovies = "</br>/{franchiseId} - Add the franchise ID to the URL to see all the movies in it";
        String deleteMovies = "</br>/delete/movieID - Delete a movie by typing in its ID";
        String updateMovies = "</br>/update/{characterId}/{fullName}/{alias}/{gender}/{picture}/{movieId} - Type in the ID of the user you want to update and fill in the data";
        String createMovies = "</br>/create/{fullName}/{alias}/{gender}/{picture}/{movieId} - Add a character to the database using this command";
        return header + intro + getMovies + getFranchiseMovies + deleteMovies + updateMovies + createMovies;
    }

    //Getting all movies
    public ArrayList<Movie> getAllMovies() {
        ArrayList<Movie> movies = new ArrayList<>();
        //Make SQL query
        jdbcTemplate.query(
                "SELECT * FROM Movie",
                (ResultSetExtractor<String>) resultSet -> {
                    while (resultSet.next()) {
                        movies.add(
                                new Movie(
                                        resultSet.getLong("id"),
                                        resultSet.getString("movie_title"),
                                        resultSet.getString("genre"),
                                        resultSet.getInt("release_year"),
                                        resultSet.getString("director"),
                                        resultSet.getString("picture"),
                                        resultSet.getString("trailer"),
                                        resultSet.getInt("franchiseid")
                                )
                        );
                    }
                    return null;
                });
        return movies;
    }

    //Getting all movies in a franchise
    public ArrayList<Movie> getAllFranchiseMovies(int franchiseId) {
        ArrayList<Movie> movies = new ArrayList<>();
        //Make SQL query
        jdbcTemplate.query(
                "SELECT * FROM Movie WHERE franchise_id = ?",
                ps -> ps.setInt(1, franchiseId),
                (ResultSetExtractor<String>) resultSet -> {
                    while (resultSet.next()) {
                        movies.add(
                                new Movie(
                                        resultSet.getLong("id"),
                                        resultSet.getString("movie_title"),
                                        resultSet.getString("genre"),
                                        resultSet.getInt("release_year"),
                                        resultSet.getString("director"),
                                        resultSet.getString("picture"),
                                        resultSet.getString("trailer"),
                                        resultSet.getInt("franchiseid")
                                )
                        );
                    }
                    return null;
                });
        return movies;
    }

    //Deleting a movie from the DB
    public String deleteMovie(int movieId) {
        jdbcTemplate.update(
                "DELETE FROM Character where id=?",
                ps -> ps.setLong(1, movieId)
        );
        return "Successfully deleted movie";
    }

    //Updating a movie from the DB
    public String updateMovie(int movieId, String movieTitle, String genre,  int releaseYear, String director, String picture, String trailer, int franchiseId) {
        String sql = "UPDATE Movie SET movie_title=?, genre=?, release_year=?, director=?, picture=?, trailer=? franchiseid=? WHERE id=?";
        jdbcTemplate.update(sql,movieTitle, genre, releaseYear, director, picture, trailer, franchiseId, movieId);
        return "Successfully updated movie";
    }

    //Creating a new movie in the DB
    public String createMovie(String movieTitle, String genre, int releaseYear, String director, String picture, String trailer, int franchiseId) {
        String sql = "INSERT INTO Movie(movie_title, genre, release_year, director, picture, trailer, franchiseId VALUES(?,?,?,?,?,?,?)";
        jdbcTemplate.update(sql, movieTitle, genre, releaseYear, director, picture, trailer, franchiseId);
        return "Successfully created movie!";
    }


    public void saveMovie(Movie movie) {
        String sql = "INSERT INTO Movie (id, movie_title, genre, release_year, director, picture, trailer,franchiseid) VALUES (?,?,?,?,?,?,?,?) ON CONFLICT DO NOTHING";
        jdbcTemplate.update(sql, movie.getId(), movie.getMovieTitle(), movie.getGenre(), movie.getReleaseYear(), movie.getDirector(), movie.getPicture(), movie.getTrailer(), movie.getFranchiseId());
    }
}
