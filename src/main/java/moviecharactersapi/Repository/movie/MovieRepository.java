package moviecharactersapi.Repository.movie;

import moviecharactersapi.models.Movie;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface MovieRepository {
    public String welcomeToMovies();
    public ArrayList<Movie> getAllMovies();
    public ArrayList<Movie> getAllFranchiseMovies(int franchiseId);
    public String deleteMovie(int movieId);
    public String updateMovie(int movieId, String movieTitle, String genre,  int releaseYear, String director, String picture, String trailer, int franchiseId);
    public String createMovie(String movieTitle, String genre, int releaseYear, String director, String picture, String trailer, int franchiseId);

    public void saveMovie(Movie movie);
}
