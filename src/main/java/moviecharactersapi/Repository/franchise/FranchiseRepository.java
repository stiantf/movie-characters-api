package moviecharactersapi.Repository.franchise;

import moviecharactersapi.models.Character;
import moviecharactersapi.models.Franchise;
import moviecharactersapi.models.Franchise;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface FranchiseRepository {
    public String welcomeToFranchise();
    public ArrayList<Franchise> getAllFranchises();
    public String deleteFranchise(int franchiseId);
    public String updateFranchise(int franchiseId, String name, String description);
    public String createFranchise(String name, String description);

    public void saveFranchise(Franchise franchise);
}
