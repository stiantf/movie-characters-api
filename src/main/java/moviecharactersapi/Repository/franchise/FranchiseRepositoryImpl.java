package moviecharactersapi.Repository.franchise;

import moviecharactersapi.Repository.franchise.FranchiseRepository;
import moviecharactersapi.models.Character;
import moviecharactersapi.models.Franchise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public class FranchiseRepositoryImpl implements FranchiseRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //Introduction to franchise page
    public String welcomeToFranchise() {
        String header = "----------- FRANCHISE PAGE -----------";
        String intro = "</br>Add the following to your URL to access commands!";
        String getAllFranchises = "</br>/all - See all franchises";
        String deleteFranchises = "</br>/delete/{franchiseId} - Delete a franchise by typing in its ID";
        String updateFranchises = "</br>/update/{franchiseId}/{name}/{description} - Type in the ID of the franchise you want to update and fill in the data";
        String createFranchises = "</br>/create/{name}/{description} - Add a franchise to the database using this command";
        return header + intro + getAllFranchises + deleteFranchises + updateFranchises + createFranchises;
    }

    //Get all franchises
    public ArrayList<Franchise> getAllFranchises() {
        ArrayList<Franchise> franchises = new ArrayList<>();
        //Make SQL query
        jdbcTemplate.query(
                "SELECT * FROM Franchise",
                (ResultSetExtractor<String>) resultSet -> {
                    while (resultSet.next()) {
                        franchises.add(
                                new Franchise(
                                        resultSet.getLong("id"),
                                        resultSet.getString("name"),
                                        resultSet.getString("description")
                                )
                        );
                    }
                    return null;
                });
        return franchises;
    }

    //Delete a franchise from the DB
    public String deleteFranchise(int franchiseId) {
            jdbcTemplate.update(
                    "DELETE FROM Franchise WHERE id = ?",
                    ps -> ps.setLong(1,franchiseId)
            );
            return "Successfully deleted Franchise";
        }

    //Update a franchise in the DB
    public String updateFranchise(int franchiseId, String name, String description) {
        String sql = "UPDATE Franchise SET name=?, description=? WHERE id=?";
        jdbcTemplate.update(sql, name, description, franchiseId);
        return "Franchise successfully updated!";
    }

    //Create a franchise in the DB
    public String createFranchise(String name, String description) {
        String sql ="INSERT INTO Franchise(name, description) VALUES(?,?)";
        jdbcTemplate.update(sql, name, description);
        return "Franchise successfully created!";
    }



    public void saveFranchise(Franchise franchise) {
        String sql = "INSERT INTO Franchise (id, name, description) VALUES (?,?,?) ON CONFLICT DO NOTHING";
        jdbcTemplate.update(sql, franchise.getId(), franchise.getName(), franchise.getDescription());

    }
}
