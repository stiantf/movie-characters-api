package moviecharactersapi.Repository.character;

import moviecharactersapi.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface CharacterRepository {


    public String welcomeToCharacters();
    public ArrayList<Character> getAllCharacters();
    public ArrayList<Character> getAllMovieCharacters(int movieId);
    public ArrayList<Character> getAllFranchiseCharacters(int franchiseId);
    public String deleteCharacter(int characterId);
    public String updateCharacter(int characterId, String fullName, String alias, String gender, String picture, int movieId);
    public String createCharacter(String fullName, String alias, String gender, String picture, int movieId);

    public void saveCharacter(Character character);

}
