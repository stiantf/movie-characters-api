package moviecharactersapi.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class CharactersInMoviesPK implements Serializable {
    @Basic(optional = false)
    @Column(name="characterid")
    private long characterid;

    @Basic(optional = false)
    @Column(name="movieid")
    private long movieid;
}
