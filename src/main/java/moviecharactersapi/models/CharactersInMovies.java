package moviecharactersapi.models;

import javax.persistence.*;
import java.io.Serializable;

@Table(name="characters_in_movies")
public class CharactersInMovies implements Serializable {
    @EmbeddedId
    protected CharactersInMoviesPK charactersInMoviesPK;
    @ManyToOne
    @JoinColumn(name="characterid", referencedColumnName = "id")
    private long characterid;
    private Character character;

    @ManyToOne(optional = false)
    @JoinColumn(name="movieid", referencedColumnName = "id")
    private long movieid;
    private Movie movie;
}
